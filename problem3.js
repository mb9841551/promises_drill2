const users = fetch(`https://jsonplaceholder.typicode.com/users`);  //fetching data
const todo = fetch(`https://jsonplaceholder.typicode.com/todos`);

users
.then((response) => {      //response method to check the incoming HTTP request
    if(!response.ok){
        throw new Error(`HTTP error! Status code: ${response.code}`);
    }
    return response.json();     //convert into json data
})
.then((data) => {
    console.log(`usersData : `,data);   //print the data
    return todo;
})
.then((response) => {
    if(!response.ok){
        throw new Error(`HTTP error! Status code: ${response.code}`);   //to check if response is correct or not
    }
    return response.json();   
})
.then((todoData) => {
    console.log(`Todo details:`,todoData)     //print the data
})
.catch((err) => {
    console.error(new Error(`Error while fetching`,err)); 
})
