const users = fetch('https://jsonplaceholder.typicode.com/users');    //fetching data

users
  .then((response) => {     //response method to check the incoming HTTP request
    if (!response.ok) {
      throw new Error('Error in fetching users data');
    }
    return response.json();   //converting into json data
  })
  .then((data) => {
    const userDetailsPromises = data.map((user) => {
      const { id, name } = user;    //destructure the object
      console.log('UserId :', id," UserName :", name);

      const specificUserDetail = fetch(`https://jsonplaceholder.typicode.com/users?id=${id}`)  //fetch data using specific id
        .then((response) => {
          if (!response.ok) {
            throw new Error(`Error in fetching details for user ${id}`);
          }
          return response.json();
        });
        return specificUserDetail;
    });  
    return Promise.all(userDetailsPromises);       //returning an array of Promises

  })
  .then((userDetails) => {
    console.log('Specific User Details:', userDetails);  //print the data
  })
  .catch((err) => {
    console.error('Error occurred:', err);
  });