const users = fetch(`https://jsonplaceholder.typicode.com/users`);  //fetched the data

users
.then((response) => {     //response method to check the incoming HTTP request
    if(!response.ok){
        throw new Error(`HTTP request error ! Status: ${response.status}`);
    }
    return response.json();  //Convert to json data
})
.then((data) => {
    console.log(`User details`,data) 
})
.catch((err) => {
    console.erroe(new Error(`Error while fetching`,err));
})
