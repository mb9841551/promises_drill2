const usersTodo = fetch(`https://jsonplaceholder.typicode.com/todos`);   //fetched the data

usersTodo
.then((response) => {     //response method to check the incoming HTTP request
    if(!response.ok){
        console.error(new Error(`HTTP request error ! Status: ${response.status}`));
    }
    return response.json();   //Convert to json data
})
.then((data) => {
    console.log(`Todo details`,data)
})
.catch((err) => {
    console.erroe(new Error(`Error while fetching`,err));
})
