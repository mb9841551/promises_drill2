const todo = fetch('https://jsonplaceholder.typicode.com/todos');    //fetch the data

todo
.then((response) => {      //response method to check the incoming HTTP request
    if(!response.ok){
        throw new Error("Somethhing wrong while fetching Todo data");
    }
    return response.json();  //converting into json data
})
.then((data) => {
    const specificTodoUser = data.filter((user) => {
        const {userId} = user;
        if(userId === 1){   //to check for first userId
            const specificTodoDetails = fetch(`https://jsonplaceholder.typicode.com/todos?userId=${userId}`)  //fetching data for specific userId
            .then((response) => {
                if(!response.ok){
                    throw new Error("Somethhing wrong while fetching specific Todo data");
                }
                return response.json();
            })
            return specificTodoDetails;   //return the promise as fetch is also a promise
        }
    })
    return Promise.all(specificTodoUser);  //returning an array of Promises
})
.then((data) => {
    console.log("Specific TO do user details :",data);  //print the data
})
.catch((err) => {
    console.error("Error occurred",err);
})